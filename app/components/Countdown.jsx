var React = require('react');
var Clock = require('Clock')
var CountdownForm = require('CountdownForm');
var Controls = require('Controls');
var Countdown = React.createClass({
	getInitialState: function() {
		return {
			count:0,
			countdownStatus: 'stopped'
		}
	},
	componentDidUpdate: function(prevProps,prevState) {
		if(this.state.countdownStatus !== prevState.countdownStatus) {
			switch (this.state.countdownStatus) {
				case 'started':
					this.startTimer();
					break;
			}
		}
	},
	startTimer: function() {
		this.timer = setInterval(() => {
			var newCount = this.state.count - 1;
			this.setState({
				count: newCount >= 0 ? newCount : 0
			})
			if (this.state.count === 0) {
				clearTimeout(this.timer);
				this.setState({
					countdownStatus: 'stopped'
				})
			}
		},1000)
	},
	handleSetCountdown: function(seconds) {
		this.setState({
			count: seconds,
			countdownStatus: 'started'
		})
	},
	handleClear: function() {
		this.setState({
			count: 0,
			countdownStatus: 'stopped'
		})
		clearTimeout(this.timer);
	},
	handlePause: function() {
		this.setState({
			countdownStatus: 'paused'
		})
		clearTimeout(this.timer);
	},
	handleStart: function() {
		this.setState({
			countdownStatus: 'started'
		})
	},
	handleControl: function() {
		var {countdownStatus} = this.state;

		if (countdownStatus === 'started') {
			return <Controls clear={this.handleClear} start={this.handleStart} pause={this.handlePause}countdownStatus={countdownStatus}/>
		} else if(countdownStatus === 'paused') {
			return <Controls clear={this.handleClear} start={this.handleStart} pause={this.handlePause}countdownStatus={countdownStatus}/>
		}
		if (countdownStatus === 'stopped') {
			return <CountdownForm onSetCountdown={this.handleSetCountdown}/>
		}
 	},
	render: function() {
		var {count,countdownStatus} = this.state;
		return (
			<div className="countdown">
				<h1>Countdown app</h1>
				<Clock totalSeconds={count}/>
				{this.handleControl()}
			</div>
		);
	}
})
module.exports = Countdown;