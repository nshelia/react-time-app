var React = require('react');

var Controls = React.createClass({
	propTypes: {
		countdownStatus: React.PropTypes.string.isRequired
	},
	handleClear: function() {
		this.props.clear();
	},
	handlePause: function() {
		this.props.pause();
	},
	handleStart: function() {
		this.props.start();
	},
	render: function() {
		var {countdownStatus} = this.props;
		var renderStartStopButton = () => {
			if (countdownStatus === 'started') {
				return <button onClick={this.handlePause} className="button secondary">Pause</button>
			} else if (countdownStatus === 'paused') {
				return <button onClick={this.handleStart} className="button primary">Start</button>
			}
		}
		return (
			<div className="controls">
				<div className="column small-6">
				{renderStartStopButton()}
				</div>
				<div className="column small-6">
				<button onClick={this.handleClear} className="button alert hollow">Clear</button>
				</div>
			</div>
		);
	}
});

module.exports = Controls;