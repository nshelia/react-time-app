var React = require('react');
var Clock = require('Clock');
var Controls = require('Controls');
var Timer = React.createClass({
	getInitialState: function() {
		return {count: 0,countdownStatus: 'paused'}
	},
	startTimer: function() {
		this.timer = setInterval(() => {
			var newCount = this.state.count + 1;
			this.setState({
				count: newCount
			})
		},1000)
	},
	handleStart: function() {
		clearTimeout(this.timer);
		this.setState({
			countdownStatus: 'started'
		})
		this.startTimer();
	},
	handlePause: function() {
		clearTimeout(this.timer);
		this.setState({
			countdownStatus: 'paused'
		})
	},
	handleClear: function() {
		clearTimeout(this.timer);
		this.setState({
			count: 0,
			countdownStatus: 'paused'
		})
	},
	render: function() {
		var {count,countdownStatus} = this.state;
		return (
			<div className="countdown">
				<h1>Timer App</h1>
				<Clock totalSeconds={count}/>
				<Controls clear={this.handleClear} start={this.handleStart} pause={this.handlePause} countdownStatus={countdownStatus}/>
			</div>
		);
	}
})
module.exports = Timer;