var React = require('react');
var ReactDOM = require('react-dom');
var expect = require('expect');
var $ = require('jQuery');
var TestUtils = require('react-addons-test-utils');

var Timer = require('Timer');

describe('Timer',() => {
	it('should exist',() => {
		expect(Timer).toExist();
	})
	describe('render',() => {
		it('should start if countdown status is started',(done) => {
			var timer = TestUtils.renderIntoDocument(<Timer />);
			expect(timer.state.countdownStatus).toBe('paused');
			timer.handleStart();
			setTimeout(() => {
				expect(timer.state.count).toBe(1);
				expect(timer.state.countdownStatus).toBe('started');
				done();
			},1001)
		})
		it('should pause if countdown status is paused',(done) => {
			var timer = TestUtils.renderIntoDocument(<Timer />);
			expect(timer.state.countdownStatus).toBe('paused');
			timer.handleStart();
			expect(timer.state.countdownStatus).toBe('started');
			setTimeout(() => {
				timer.handlePause();
				expect(timer.state.count).toBe(1);
				expect(timer.state.countdownStatus).toBe('paused');
				setTimeout(() => {
					expect(timer.state.count).toBe(1);
					done();
				},2001)
			},1001)
			
		})
		it('should pause if clear button is clicked',() => {
			var timer = TestUtils.renderIntoDocument(<Timer />);
			timer.handleClear();
			expect(timer.state.countdownStatus).toBe('paused');
			expect(timer.state.count).toBe(0);
		})
	})
})